#include "MineralWater.h"

MineralWater::MineralWater(const std::string &title, double volume, double concentration, const std::string &mineral)
        : SparkledDrink(title, volume, concentration), mineral(mineral) {}
