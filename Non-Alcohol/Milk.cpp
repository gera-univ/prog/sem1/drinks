#include "Milk.h"

Milk::Milk(const std::string &title, double volume, MilkingAnimal animal) : NonAlcoholDrink(title, volume),
                                                                            animal(animal) {}
