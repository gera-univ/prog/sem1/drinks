#ifndef DRINKS_MINERALWATER_H
#define DRINKS_MINERALWATER_H

#include "../SparkledDrink.h"

class MineralWater : public SparkledDrink {
    std::string mineral;
public:
    MineralWater(const std::string &title, double volume, double concentration, const std::string &mineral);
};


#endif //DRINKS_MINERALWATER_H
