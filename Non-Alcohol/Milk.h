#ifndef DRINKS_MILK_H
#define DRINKS_MILK_H

#include <string>
#include "NonAlcoholDrink.h"
#include "../Enums/MilkingAnimal.h"

class Milk : public NonAlcoholDrink {
    MilkingAnimal animal;
public:
    Milk(const std::string &title, double volume, MilkingAnimal animal);
};


#endif //DRINKS_MILK_H
