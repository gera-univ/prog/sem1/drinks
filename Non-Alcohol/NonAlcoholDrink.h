#ifndef DRINKS_NONALCOHOLDRINK_H
#define DRINKS_NONALCOHOLDRINK_H

#include "../BottledDrink.h"

class NonAlcoholDrink : public BottledDrink {
public:
    NonAlcoholDrink(const std::string &title, double volume);
};


#endif //DRINKS_NONALCOHOLDRINK_H
