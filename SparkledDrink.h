#ifndef DRINKS_SPARKLEDDRINK_H
#define DRINKS_SPARKLEDDRINK_H

#include "BottledDrink.h"

class SparkledDrink : public BottledDrink {
    double concentration;

public:
    SparkledDrink(const std::string &title, double volume, double concentration);

};


#endif //DRINKS_SPARKLEDDRINK_H
