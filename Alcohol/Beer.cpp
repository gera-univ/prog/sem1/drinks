#include "Beer.h"

Beer::Beer(const std::string &title, double volume, double proof, const BeerMaterial &material) : AlcoholDrink(title,
                                                                                                              volume,
                                                                                                              proof),
                                                                                                 material(material) {}
