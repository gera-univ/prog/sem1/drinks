#ifndef DRINKS_ALCOHOLDRINK_H
#define DRINKS_ALCOHOLDRINK_H

#include "../BottledDrink.h"

class AlcoholDrink : public BottledDrink {
    double proof; // крепкость

public:
    AlcoholDrink(const std::string &title, double volume, double proof);

};


#endif //DRINKS_ALCOHOLDRINK_H
