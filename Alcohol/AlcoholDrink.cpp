#include "AlcoholDrink.h"

AlcoholDrink::AlcoholDrink(const std::string &title, double volume, double proof) : BottledDrink(title, volume),
                                                                                    proof(proof) {}
