#ifndef DRINKS_BEER_H
#define DRINKS_BEER_H

#include "AlcoholDrink.h"
#include "../Enums/BeerMaterial.h"

class Beer  : public AlcoholDrink {
    Beer(const std::string &title, double volume, double proof, const std::string &material);

    BeerMaterial material;

public:
    Beer(const std::string &title, double volume, double proof, const BeerMaterial &material);
};


#endif //DRINKS_BEER_H
