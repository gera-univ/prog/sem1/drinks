#include "SparkledDrink.h"

SparkledDrink::SparkledDrink(const std::string &title, double volume, double concentration) : BottledDrink(title,
                                                                                                           volume),
                                                                                              concentration(
                                                                                                      concentration) {}
