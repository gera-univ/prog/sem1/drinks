#ifndef DRINKS_BOTTLEDDRINK_H
#define DRINKS_BOTTLEDDRINK_H

#include <string>

class BottledDrink {
    std::string title;
    double volume;

public:
    BottledDrink(const std::string &title, double volume);
};


#endif //DRINKS_BOTTLEDDRINK_H
