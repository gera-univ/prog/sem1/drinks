#ifndef DRINKS_BEERMATERIAL_H
#define DRINKS_BEERMATERIAL_H

enum BeerMaterial {
    Wheat, Barley, Rye
};
#endif //DRINKS_BEERMATERIAL_H
